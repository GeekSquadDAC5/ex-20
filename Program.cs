﻿using System;

namespace ex_20
{
    class Program
    {
        static void Main(string[] args)
        {
            // --------------------------------------------
            // Exercise #20 - Simple Decisions
            // --------------------------------------------
            // Note: You only need a single repository for this task - everything can be coded in a single Program.cs
            // Note 2: When you go on to the next question, watch out for the names of the variables.
            // Note 3: If it helps with your program flow, write out a flow diagram to help you with your program

            var num1 = 0;
            var num2 = 0;

            // a)
            // Write a program that will compare 2 numbers
            // These numbers are to be entered by the user
            Console.WriteLine("-----------------------------------");
            Console.WriteLine(" Task A");
            Console.WriteLine("-----------------------------------");
            Console.Write("Please type in a number : ");
            num1 = int.Parse(Console.ReadLine());
            Console.Write("Please type in an another number : ");
            num2 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            if(num1 == num2)
            {
                Console.WriteLine("-----------------------------------");
                Console.WriteLine("Number 1 is the same as number 2");
                Console.WriteLine("-----------------------------------");
            }
            else if(num1 > num2)
            {
                Console.WriteLine("-----------------------------------");
                Console.WriteLine("Number 1 is greater than number 2");
                Console.WriteLine("O123r number 2 is less than number 1");
                Console.WriteLine("-----------------------------------");
            }
            else 
            {
                Console.WriteLine("-----------------------------------");
                Console.WriteLine("Number 1 is less than number 2");
                Console.WriteLine("Or number 2 is greater than number 1");
                Console.WriteLine("-----------------------------------");
            }

            // b) 
            // Using .Length after a string variable returns the length of the string in numbers
            // Imagine you are writing a password requirement field and the only requirement is 
            // that the password needs to be at least 8 characters
            // Long.
            // If the password is less than 8 characters long tell the user that the password is too short.

            string password;

            Console.WriteLine("\n\n");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine(" Task B");
            Console.WriteLine("-----------------------------------");
            Console.Write("Please type your password in : ");
            password = Console.ReadLine();
            Console.WriteLine();

            if(password.Length < 8)
            {
                Console.WriteLine("The password is too short.\n");
                Console.WriteLine("The password must be greater than 8 characters long.\n");
            }
            else 
            {
                Console.WriteLine("Thank you for your password. :) \n");
            }

            // c)
            // Using .Replace(“old value”,”new value”) we are able to change the value of a string.
            // Create a little program that allows you to change the password, check that the password is
            // actually changing, and that the user does not use the same password

            string oldPassword;
            string newPassword;

            Console.WriteLine("\n\n");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine(" Task C");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("Please change your password");
            Console.WriteLine();
            Console.Write("> Type in your old password : ");
            oldPassword = Console.ReadLine();
            password = oldPassword;
            Console.WriteLine($"> Your old password is '{password}'");
            
            Console.WriteLine();
            Console.Write("> Type in your new password : ");
            newPassword = Console.ReadLine();
            password = password.Replace(oldPassword, newPassword);
            Console.WriteLine($"> Your new password is '{password}'");
            Console.WriteLine("-----------------------------------");
        }
    }
}
